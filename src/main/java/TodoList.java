import java.util.ArrayList;

public class TodoList {
    private ArrayList<Task> taskList = new ArrayList<>();

    public boolean addTask(String name, boolean status){
        Task task = new Task();
        task.setName(name);
        task.setStatus(status);
        task.setId(this.getToDoListSize()+1);
        taskList.add(task);
        return true;
    }

    //task should return string of name and status
    public String getTask(int index){
        String ret = "";
        int id = taskList.get(index).getId();
        String name = taskList.get(index).getName();

        boolean status = taskList.get(index).getStatus();
        if(status) {
            ret = ret + id  + ". " + name + " [Done]";
        } else{
            ret = ret + id + ". " + name + " [Not Done]";
        }
        return ret;
    }

    //todolist should return string of list of tasks separated by new line
    public String getToDoList(){
        String ret = "";
        for (Task task : taskList) {
            int id = task.getId();
            String name = task.getName();

            boolean status = task.getStatus();
            if (status) {
                ret = ret + id + ". " + name + " [Done]" + "\n";
            } else {
                ret = ret + id + ". " + name + " [Not Done]" + "\n";
            }
        }

        return ret;
    }

    public int getToDoListSize(){
        return taskList.size();
    }
}

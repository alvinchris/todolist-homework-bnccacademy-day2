import org.junit.Test;

import static org.junit.Assert.*;

public class TodoListTest {

    @Test
    public void getTaskReturnNameAndStatus(){
        TodoList todo = new TodoList();
        todo.addTask("Study Java", true);
        todo.addTask("Learn TDD", false);
        assertEquals("1. Study Java [Done]", todo.getTask(0));
    }

    @Test
    public void getToDoListReturnAllOfTasks() {
        TodoList todo = new TodoList();
        todo.addTask("Study Java", true);
        todo.addTask("Learn TDD", false);

        assertEquals("1. Study Java [Done]\n2. Learn TDD [Not Done]\n", todo.getToDoList());
    }

    @Test
    public void getToDoListSizeTest(){
        TodoList todo = new TodoList();
        todo.addTask("Study Java", true);
        todo.addTask("Learn TDD", false);
        assertEquals(2, todo.getToDoListSize());
    }

    @Test
    public void addTaskReturnTrue(){
        TodoList todo = new TodoList();
        boolean actual = todo.addTask("Study Java", true);;
        assertTrue(actual);
    }
}

